import React from 'react';
import ReactDOM from 'react-dom';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';

import i18next from 'i18next';

class Navigation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: document.querySelector('#navbar').dataset.user,
    }

    this.user = this.user.bind(this);
    this.setLang = this.setLang.bind(this);
  }

  user () {
    if(this.state.username == 'none') {
      return <Nav.Link href="/login" style={{ color: '#ffffff' }}>Login</Nav.Link>
    } else {
      return (
        <>
          <Nav.Link href="/admin" style={{ color: '#ffffff' }}>Admin</Nav.Link>
          <Nav.Link href="/logout" style={{ color: '#ffffff' }}>Logout</Nav.Link>
        </>
      )
    }
  }

  setLang(e) {
    i18next.changeLanguage(e);
    window.location.reload();
  }

  render () {
    return (
      <Navbar variant="dark" expand="lg" style={{ backgroundColor: '#151554' }}>
        <Container>
          <Navbar.Brand href="/">
            <img
              src="/logo.svg"
              width="200"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto justify-content-end" style={{ width: "100%" }}>
              <NavDropdown title={i18next.language} id="nav-dropdown" onSelect={this.setLang}>
                <NavDropdown.Item eventKey="en">English</NavDropdown.Item>
                <NavDropdown.Item eventKey="de">Deutsch</NavDropdown.Item>
              </NavDropdown>
              <Nav.Link href="/" style={{ color: '#ffffff' }}>{i18next.t('nav_home')}</Nav.Link>
              <Nav.Link href="/about" style={{ color: '#ffffff' }}>{i18next.t('nav_about')}</Nav.Link>
              <Nav.Link href="/contact" style={{ color: '#ffffff' }}>{i18next.t('nav_contact')}</Nav.Link>
              {this.user()}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    )
  }
}

ReactDOM.render(<Navigation/>, document.getElementById('navbar'));
