import React from 'react';
import ReactDOM from 'react-dom';

import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import { Doughnut } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement
);

class Admin extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div>
        <Header/>
        <WebsiteHits/>
        <UserAgent/>
        <Subscribers/>
      </div>
    )
  }
}

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: document.querySelector('#admin').dataset.name,
    }
  }

  render () {
    return (
      <Container style={{ marginTop: '20px' }}>
        <p>You are logged in as <span style={{ textDecoration: 'underline' }}>{this.state.name}</span></p>
      </Container>
    )
  }
}

class Subscribers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      entries: null,
    }

    this.getData = this.getData.bind(this);
    this.download = this.download.bind(this);
    this.exportSubscribers = this.exportSubscribers.bind(this);
    this.getData();
  }

  async getData() {
    const response = await fetch('/admin/api');
    const data = await response.json();
    this.setState({
      entries: data,
    });
  }

  download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  exportSubscribers () {
    var text = "";
    this.state.entries.map(entry => (
      text += entry.name + ',' + entry.email + '\n'
    ))
    var filename = "subscribers.csv";
      
    this.download(filename, text);
  }

  table () {
    if(this.state.entries == null || this.state.entries.legnth == 0) {
      return (
        <Container style={{ marginTop: '50px' }}>
          <Alert severity="success">There are no subscribers for the newsletter</Alert> 
        </Container>
      );
    } else {
      return (
        <Container style={{ marginTop: '50px' }}>
          <h1>Newsletter Subscribers</h1>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {this.state.entries.map(entry => (
                <tr key={entry.id}>
                  <td>{entry.id}</td>
                  <td>{entry.name}</td>
                  <td>{entry.email}</td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Button onClick={this.exportSubscribers}>
            Download .csv
          </Button>
        </Container>
      ) 
    }
  }

  render () {
    return (
      this.table()
    )
  }
}

class WebsiteHits extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hits: '',
      main: '',
      about: '',
      contact: '',
      labels: '',
    };
    this.getData = this.getData.bind(this);
    this.sinceStart = this.sinceStart.bind(this);
    this.lastWeek = this.lastWeek.bind(this);
    this.lastDay = this.lastDay.bind(this);
    this.lastWeek();
  }

  async getData(days) {
    const response = await fetch('/admin/websiteHits/' + days);
    const data = await response.json();
    this.setState({
      hits: data.hits,
      main: data.main,
      about: data.about,
      contact: data.contact,
      labels: data.labels,
    });
  }

  sinceStart () {
    const d = new Date(2021, 10, 22);
    const today = new Date();
    const days = Math.ceil((today - d + 1) / 86400000);
    this.getData(days);
  }

  lastWeek () {
    this.getData(7);
  }

  lastDay () {
    this.getData(1);
  }

  render() {
    if(this.state.hits.length != 0) {
      const options = {
        responsive: true,
        plugins: {
          legend: {
            position: 'bottom',
          },
        },
      };
      let labels = this.state.labels;
      let hits = Object.values(this.state.hits);
      let main = Object.values(this.state.main);
      let about = Object.values(this.state.about);
      let contact = Object.values(this.state.contact);

      const data = {
        labels: labels,
        datasets : [
          {
            label: 'Website Hits',
            data: hits,
            borderColor: '#151554',
            backgroundColor: '#c4c4d1',
          },
          {
            label: 'Main Hits',
            data: main,
            borderColor: '#24c421',
            backgroundColor: '#c4c4d1',
          },
          {
            label: 'About Hits',
            data: about,
            borderColor: '#ea4f4f',
            backgroundColor: '#c4c4d1',
          },
          {
            label: 'Contact Hits',
            data: contact,
            borderColor: '#e2ed1c',
            backgroundColor: '#c4c4d1',
          }
        ]
      }

      return (
        <Container>
          <h1>Website Hits</h1>
          <Line options={options} data={data} />
          <Row style={{ marginTop: '10px', textAlign: 'center' }}>
            <Col sm>
              <Button onClick={this.sinceStart}>
                Since Start
              </Button>
            </Col>
            <Col sm>
              <Button onClick={this.lastWeek}>
                Last 7 Days
              </Button>
            </Col>
            <Col sm>
              <Button onClick={this.lastDay}>
                Last 24 Hours
              </Button>
            </Col>
          </Row>
        </Container>
      );
    } else {
      return (
        <Container>
        </Container>
      );
    }
  }
}

class UserAgent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      browser: '',
      OS: '',
      ready: false,
    }
    this.getData = this.getData.bind(this);

    this.getData();
  }

  async getData() {
    const response = await fetch('/admin/userAgent');
    const data = await response.json();
    this.setState({
      browserLabels: data.BrowserKeys,
      browserValues: data.BrowserValues,
      OSLabels: data.OSKeys,
      OSValues: data.OSValues,
      ready: true,
    });
  }

  render() {
    if(!this.state.ready) {
      return(
        <Container>
        </Container>
      )
    } else {
      let dataB = {
        labels: this.state.browserLabels,
        datasets: [
          {
            data: this.state.browserValues,
            backgroundColor: [
              'rgba(215,48,39,0.5)',
              'rgba(252,141,89,0.5)',
              'rgba(254,224,114,0.5)',
              'rgba(255,255,191,0.5)',
              'rgba(224,243,248,0.5)',
              'rgba(145,191,219,0.5)',
              'rgba(69,117,180,0.5)',
            ],
            borderColor: [
              'rgba(215,48,39,1)',
              'rgba(252,141,89,1)',
              'rgba(254,224,114,1)',
              'rgba(255,255,191,1)',
              'rgba(224,243,248,1)',
              'rgba(145,191,219,1)',
              'rgba(69,117,180,1)',
            ]
          }
        ],
        hoverOffset: 4
      };
      let dataOS = {
        labels: this.state.OSLabels,
        datasets: [
          {
            data: this.state.OSValues,
            backgroundColor: [
              'rgba(215,48,39,0.5)',
              'rgba(252,141,89,0.5)',
              'rgba(254,224,114,0.5)',
              'rgba(255,255,191,0.5)',
              'rgba(224,243,248,0.5)',
              'rgba(145,191,219,0.5)',
              'rgba(69,117,180,0.5)',
            ],
            borderColor: [
              'rgba(215,48,39,1)',
              'rgba(252,141,89,1)',
              'rgba(254,224,114,1)',
              'rgba(255,255,191,1)',
              'rgba(224,243,248,1)',
              'rgba(145,191,219,1)',
              'rgba(69,117,180,1)',
            ],
            hoverOffset: 4
          }
        ]
      };
      const options = {
        plugins: {
          legend: {
            position: 'right',
          }
        }
      };
      return (
        <Container style={{marginTop: '20px' }}>
          <h1>User Agent</h1>
          <Row>
            <Col sm>
              <Doughnut data={dataB} options={options}/>
            </Col>
            <Col sm>
              <Doughnut data={dataOS} options={options}/>
            </Col>
          </Row>
        </Container>
      )
    }
  }
}

ReactDOM.render(<Admin/>, document.getElementById('admin'));
