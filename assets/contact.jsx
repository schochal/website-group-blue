import React from 'react';
import ReactDOM from 'react-dom';

import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import axios from 'axios';

import i18next from 'i18next';

class Contact extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <Container style={{ marginTop: '50px' }}>
        <ContactForm/>
        <div id="alert" style={{ marginBottom: '50px' }}></div>

        <NewsletterForm/>
        <div id="alertN" style={{ marginBottom: '50px' }}></div>

        <Unsubscribe/>
        <div id="alertU" style={{ marginBottom: '50px' }}></div>
      </Container>
    )
  }
}

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeMessage = this.onChangeMessage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      email: '',
      message: '',
    }
  }

  onChangeEmail() {
    let newState = {
      email: event.target.value,
      message: this.state.message,
    }
    this.setState(newState);
  }

  onChangeMessage() {
    let newState = {
      email: this.state.email,
      message: event.target.value,
    }
    this.setState(newState);
  }

  handleSubmit() {
    if(!this.state.email.includes('@') || !this.state.email.includes('.')) {
      // email not valid
      ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='danger'>This Email Address is invalid.</Alert>, document.getElementById('alert'));
    } else if (this.state.message == '') {
      ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='danger'>Your Message is empty.</Alert>, document.getElementById('alert'));
    } else {
      // send an email to all group blue members
      axios.put('/contact/api', {
        email: this.state.email,
        message: this.state.message
      }).then(response => {
        ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='success'>Your Message has been transferred. </Alert>, document.getElementById('alert'));
        this.setState({email: '', message: ''});
      })
    }
  }

  render () {
    return (
      <div>
        <h1>{i18next.t('contact_title')}</h1>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>{i18next.t('contact_email')}</Form.Label>
            <Form.Control type="email" placeholder={i18next.t('contact_emailplaceholder')} onChange={this.onChangeEmail} value={this.state.email} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicMessage">
            <Form.Label>{i18next.t('contact_message')}</Form.Label>
            <Form.Control as="textarea" rows={5} placeholder={i18next.t('contact_messageplaceholder')} onChange={this.onChangeMessage} value={this.state.message} />
          </Form.Group>
          <Button variant="primary" onClick={this.handleSubmit}>
            {i18next.t('contact_submit')}
          </Button>
        </Form>
      </div>
    )
  }
}

class NewsletterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
  }

  onChangeName() {
    let newState = {
      name: event.target.value,
      email: this.state.email
    }
    this.setState(newState);
  }

  onChangeEmail() {
    let newState = {
      name: this.state.name,
      email: event.target.value,
    }
    this.setState(newState);
  }

  handleSubmit() {
    if(this.state.name == '') {
      // name empty
      ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='danger'>You have to enter your name.</Alert>, document.getElementById('alertN'));
    } else if(!this.state.email.includes('@') || !this.state.email.includes('.')) {
      // email not valid
      ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='danger'>This Email Address is invalid.</Alert>, document.getElementById('alertN'));
    } else {
      // send an email to all group blue members
      axios.put('/contact/apiN', {
        email: this.state.email,
        name: this.state.name
      }).then(response => {
        ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='success'>{response.data}</Alert>, document.getElementById('alertN'));
        this.setState({email: '', name: ''});
      })
    }
  }

  render () {
    return (
      <div>
        <h1 id="Newsletter">{i18next.t('contact_newsletter')}</h1>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicName">
            <Form.Label>{i18next.t('contact_name')}</Form.Label>
            <Form.Control type="text" placeholder={i18next.t('contact_nameplaceholder')} onChange={this.onChangeName} value={this.state.name} />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>{i18next.t('contact_email')}</Form.Label>
            <Form.Control type="email" placeholder={i18next.t('contact_emailplaceholder')} onChange={this.onChangeEmail} value={this.state.email} />
          </Form.Group>
          <Button variant="primary" onClick={this.handleSubmit}>
            {i18next.t('contact_submit')}
          </Button>
        </Form>
      </div>
    )
  }
}

class Unsubscribe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: document.querySelector('#contact').dataset.unsubscribe,
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    let newState = {
      email: event.target.value
    }
    this.setState(newState);
  }

  handleSubmit() {
    if(!this.state.email.includes('@') || !this.state.email.includes('.')) {
      // email not valid
      ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='danger'>This Email Address is invalid.</Alert>, document.getElementById('alertU'));
    } else {
      // send an email to all group blue members
      axios.put('/contact/apiU', {
        email: this.state.email,
      }).then(response => {
        ReactDOM.render(<Alert style={{ marginTop: '20px' }} variant='success'>{response.data}</Alert>, document.getElementById('alertU'));
        this.setState({email: ''});
      })
    }
  }

  render () {
    return (
      <div>
        <h1 id="unsubscribe">{i18next.t('contact_unsubscribe')}</h1>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>{i18next.t('contact_email')}</Form.Label>
            <Form.Control type="email" placeholder={i18next.t('contact_emailplaceholder')} onChange={this.onChange} value={this.state.email} />
          </Form.Group>
          <Button variant="primary" onClick={this.handleSubmit}>
            {i18next.t('contact_submit')}
          </Button>
        </Form>
      </div>
    )
  }
}


ReactDOM.render(<Contact/>, document.getElementById('contact'));
