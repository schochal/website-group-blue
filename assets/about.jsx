import React from 'react';
import ReactDOM from 'react-dom';

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import i18next from 'i18next';

class About extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div>
        <Header/>
        <Members/>
      </div>
    )
  }
}

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div style={{ backgroundColor: '#c4c4d1', paddingTop: '50px', paddingBottom: '50px', marginBottom: '50px' }}>
        <Container>
          <h1>{i18next.t('about_title')}</h1>
          <p>{i18next.t('about_titletext')}</p>
        </Container>
      </div>
    )
  }
}

class Members extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <h1>{i18next.t('about_meet')}</h1>
        <Member name={"Dmitriy Pivovarov"} image={"images/dima.jpg"} role={i18next.t('about_dimarole')} desc={i18next.t('about_dimatext')}/>
        <Member name={"Alvaro Garcia"} image={"images/alvaro.jpg"} role={i18next.t('about_alvarorole')} desc={i18next.t('about_alvarotext')}/>
        <Member name={"Giorgio Belleri"} image={"images/giorgio.jpg"} role={i18next.t('about_giorgiorole')} desc={i18next.t('about_giorgiotext')}/>
        <Member name={"Alexander Schoch"} image={"images/alex.png"} role={i18next.t('about_alexrole')} desc={i18next.t('about_alextext')}/>
        <Row>
          <Col sm>
            <Card>
              <Card.Img variant="left" src="images/stark.png" />
              <Card.Body>
                <Card.Title>Prof. Dr. Wendelin Stark</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{i18next.t('about_advisor')}</Card.Subtitle>
              </Card.Body>
            </Card>
          </Col>
          <Col sm>
            <Card>
              <Card.Img variant="left" src="https://ethz.ch/content/eth_cache/people/4/8/6/227684/_5/en/_jcr_content/par/greybox_textimage/par/textimage/image.imageformat.textdouble.609623369.jpg" />
              <Card.Body>
                <Card.Title>Temiraliuly Damir</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{i18next.t('about_advisor')}</Card.Subtitle>
              </Card.Body>
            </Card>
          </Col>
          <Col sm>
            <Card>
              <Card.Img variant="left" src="https://ethz.ch/content/eth_cache/people/9/0/1/230109/_5/en/_jcr_content/par/greybox/par/textimage/image.imageformat.textdouble.1415336160.jpg"/>
              <Card.Body>
                <Card.Title>Schweinfurth Shimon</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{i18next.t('about_advisor')}</Card.Subtitle>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}

class Member extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    if( window.innerWidth > 1280 ) {
      return (
        <Container>
          <Card style={{ width: '100%', alignItems: 'center', flexDirection: 'row', marginBottom: '20px', borderRadius: '20px' }}>
            <Card.Img style={{ width: '20%', borderRadius: '20px 0 0 20px' }} variant="left" src={this.props.image} />
            <Card.Body>
              <Card.Title><h3>{this.props.name}</h3></Card.Title>
              <Card.Subtitle className="mb-2 text-muted"><h5>{this.props.role}</h5></Card.Subtitle>
              <Card.Text>
                {this.props.desc}
              </Card.Text>
            </Card.Body>
          </Card>
        </Container>
      )
    } else {
      return (
        <Container>
          <Card style={{ width: '100%', marginBottom: '20px', borderRadius: '20px' }}>
            <Card.Img style={{ borderRadius: '20px 20px 0 0', width: '100%', height: 'auto' }} variant="left" src={this.props.image} />
            <Card.Body>
              <Card.Title><h3>{this.props.name}</h3></Card.Title>
              <Card.Subtitle className="mb-2 text-muted"><h5>{this.props.role}</h5></Card.Subtitle>
              <Card.Text>
                {this.props.desc}
              </Card.Text>
            </Card.Body>
          </Card>
        </Container>
      ) 
    }
  }
}

ReactDOM.render(<About/>, document.getElementById('about'));
