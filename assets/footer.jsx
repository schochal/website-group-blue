import React from 'react';
import ReactDOM from 'react-dom';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import i18next from 'i18next';

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <footer style={{ backgroundColor: '#151554', paddingTop: '50px', paddingBottom: '50px', marginTop: '50px', color: 'white' }}>
        <Container>
          <Row style={{ borderBottom: '1px solid white', paddingBottom: '20px' }}>
            <Col sm>
              <div>
                <h4>{i18next.t('footer_address')}</h4>
                Group Blue <br/>
                Chemical Product Design <br/><br/>
                Functional Materials Laboratory <br/>
                HCI E 109 <br/>
                Vladimir-Prelog-Weg 1-5/10 <br/>
                ETH Zürich<br/>
                8093 Zürich <br/><br/>
                Switzerland
              </div>
            </Col>
            { window.innerWidth > 1280 ?
              <Col style={{ textAlign: 'center' }} sm>
                <h4>Social Media</h4>
                <p>{i18next.t('footer_socialmediatext')} <a href='/contact#Newsletter'>Newsletter</a>.</p>
              </Col> 
              : 
              <Col style={{ marginTop: '20px' }} sm>
                <h4>Social Media</h4>
                <p>{i18next.t('footer_socialmediatext')} <a href='/contact#Newsletter'>Newsletter</a>.</p>
              </Col>
            }
            { window.innerWidth > 1280 ?
              <Col style={{ textAlign: 'right' }} sm>
                <h4>Links</h4>
                <a href="https://fml.ethz.ch/lectures/ChemProdDes.html" style={{ color: 'white' }}>Chemical Product Design</a><br/>
                <a href="https://fml.ethz.ch/" style={{ color: 'white' }}>Functional Materials Laboratory</a>
              </Col>
              :
              <Col style={{ margtinTop: '20px' }} sm>
                <h4>Links</h4>
                <a href="https://fml.ethz.ch/lectures/ChemProdDes.html" style={{ color: 'white' }}>Chemical Product Design</a><br/>
                <a href="https://fml.ethz.ch/" style={{ color: 'white' }}>Functional Materials Laboratory</a>
              </Col>
            }
          </Row>
          <Row style={{ marginTop: '20px' }}>
            <Col style={{ textAlign: 'center' }}>
              &#169; {new Date().getFullYear()}, G. Belleri, A. Garcia, D. Pivovarov, A. Schoch
            </Col>
          </Row>
        </Container>
      </footer>
    )
  }
}

ReactDOM.render(<Footer/>, document.getElementById('footer'));

