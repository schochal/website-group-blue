import React from 'react';
import ReactDOM from 'react-dom';

import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

import i18next from 'i18next';

class Main extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div>
        <Intro/>
        <Body/>
      </div>
    )
  }
}

class Intro extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    if(window.innerWidth > 1280) {
      return (
        <div style={{ backgroundColor: '#151554', paddingTop: '50px', paddingBottom: '50px', color: 'white', height: '100vh' }}>
          <Container>
            <Row style={{ alignItems: 'center', top: '50%', transform: 'translateY(50%)' }}>
              <Col sm={4}>
                <Card style={{ borderRadius: '20px' }}>
                  <Card.Img style={{ borderRadius: '20px' }} variant="top" src="images/stock_image_blue_2.png" />
                </Card>
              </Col>
              <Col sm={8} style={{ marginTop: '20px' }}>
                <h1>{i18next.t('main_title')}</h1>
                <h2>{i18next.t('main_introducing')}</h2>
                <p>{i18next.t('main_titletext')}</p>
                <p>{i18next.t('main_visionary')}</p>
              </Col>
            </Row>
          </Container>
        </div>
      )
    } else {
      return (
        <div style={{ backgroundColor: '#151554', paddingTop: '50px', paddingBottom: '50px', color: 'white' }}>
          <Container>
            <Row style={{ alignItems: 'center' }}>
              <Col sm={4}>
                <Card style={{ borderRadius: '20px' }}>
                  <Card.Img style={{ borderRadius: '20px' }} variant="top" src="images/stock_image_blue_2.png" />
                </Card>
              </Col>
              <Col sm={8} style={{ marginTop: '20px' }}>
                <h1>{i18next.t('main_title')}</h1>
                <h2>{i18next.t('main_introducing')}</h2>
                <p>{i18next.t('main_titletext')}</p>
                <p>{i18next.t('main_visionary')}</p>
              </Col>
            </Row>
          </Container>
        </div>
      )
    }
  }
}

class Body extends React.Component {
  constructor(props) {
    super(props);

    this.contact = this.contact.bind(this);
  }

  contact() {
    window.location = "/contact";
  }

  render () {
    return (
      <Container style={{ marginTop: '50px' }}>
        <Title text={i18next.t('main_why')}/>
        <h2>{i18next.t('main_omnirep')}</h2>
        <p>{i18next.t('main_whytext')}</p>
        <Row>
          <Col sm>
            <Card style={{ borderRadius: '20px', border: '2px solid #151554', marginTop: '10px' }}>
              <Card.Img variant="top" src="images/clean.svg" />
              <Card.Body>
                <Card.Title><h2>{i18next.t('main_lesscleaning')}</h2></Card.Title>
                <Card.Text>
                  <p>{i18next.t('main_lesscleaningtext')}</p>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col sm>
            <Card style={{ borderRadius: '20px', border: '2px solid #151554', marginTop: '10px'  }}>
              <Card.Img variant="top" src="images/water.svg" />
              <Card.Body>
                <Card.Title><h2>{i18next.t('main_lessfogging')}</h2></Card.Title>
                <Card.Text>
                  <p>{i18next.t('main_lessfoggingtext')}</p>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col sm>
            <Card style={{ borderRadius: '20px', border: '2px solid #151554', marginTop: '10px'  }}>
              <Card.Img variant="top" src="images/fingerprint.svg" />
              <Card.Body>
                <Card.Title><h2>{i18next.t('main_lessgrease')}</h2></Card.Title>
                <Card.Text>
                  <p>{i18next.t('main_lessgreasetext')}</p>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Title text={i18next.t('main_how')}/>
        <Row style={{ alignItems: 'center' }}>
          <Col sm={4}>
            <Card style={{ borderRadius: '20px' }}>
              <Card.Img style={{ borderRadius: '20px' }} variant="top" src="images/foggy_glass.png" />
            </Card>
          </Col>
          <Col sm={8}>
            <h2>{i18next.t('main_antifogging')}</h2>
            <p>{i18next.t('main_antifoggingtext')}</p>
          </Col>
        </Row>

        {window.innerWidth > 1280 ?
        <Row style={{ alignItems: 'center', marginTop: '50px' }}>
          <Col sm={8} style={{ textAlign: 'right' }}>
            <h2>{i18next.t('main_antisticking')}</h2>
            <p>{i18next.t('main_antistickingtext')}</p>
          </Col>
          <Col sm={4}>
            <Card style={{ borderRadius: '20px' }}>
              <Card.Img style={{ borderRadius: '20px' }} variant="top" src="images/drop_oilonly.png" />
            </Card>
          </Col>
        </Row>
          :
        <Row style={{ alignItems: 'center', marginTop: '50px' }}>
          <Col sm={4}>
            <Card style={{ borderRadius: '20px' }}>
              <Card.Img style={{ borderRadius: '20px' }} variant="top" src="images/drop_oilonly.png" />
            </Card>
          </Col>
          <Col sm={8}>
            <h2>{i18next.t('main_antisticking')}</h2>
            <p>{i18next.t('main_antistickingtext')}</p>
          </Col>
        </Row>
        }
        
        <Title text={i18next.t('main_wanttoknow')}/>
        <div style={{ textAlign: 'center' }}>
          <p>{i18next.t('main_wanttoknowtext')}</p>
          <Button variant="primary" size="lg" onClick={this.contact}>
            {i18next.t('main_wanttoknowbutton')}
          </Button>
        </div>
      </Container>
    )
  }
}

class Title extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1 style={{ width: '100%', textAlign: 'center', borderBottom: '2px solid #151554', lineHeight: '0em', margin: '50px 0 50px' }}>
          <span style={{ background: '#fff', padding: '0 20px' }}>
            {this.props.text}
          </span>
        </h1>
      </div>
    )
  }
}

ReactDOM.render(<Main/>, document.getElementById('main'));
