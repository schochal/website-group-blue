<?php

namespace App\Controller;

use App\Entity\WebsiteHit;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AnalyticsController extends AbstractController
{
    public function index(EntityManagerInterface $em, Request $request, string $site): void
    {
        $wh = new WebsiteHit();
        $wh->setTimestamp(new \DateTime('NOW'));
        $wh->setUserAgent($request->headers->get('user-agent'));
        $wh->setSite($site);

        $em->persist($wh);
        $em->flush();
                    
        return;
    }
}
