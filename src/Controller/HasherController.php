<?php

namespace App\Controller;

use App\Form\HasherType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HasherController extends AbstractController
{
    #[Route('/hasher', name: 'hasher')]
    public function index(Request $request): Response
    {
      $form = $this->createForm(HasherType::class);

      $hash = '';

      $form->handleRequest($request);

      if($form->isSubmitted()) {
        $pw = $form->getData()['password'];
        $hash = password_hash($pw, PASSWORD_BCRYPT, ['cost' => 13]) . PHP_EOL;
      }
        
      return $this->render('hasher/index.html.twig', [
        'form' => $form->createView(),
        'hash' => $hash, 
      ]);
    }
}
