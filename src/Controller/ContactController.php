<?php

namespace App\Controller;

use App\Controller\AnalyticsController;
use App\Entity\Subscriber;
use App\Repository\SubscriberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact')]
    public function index(Request $request): Response
    {
        $analytics = new AnalyticsController();
        $analytics->index($this->getDoctrine()->getManager(), $request, 'contact'); 

        $unsubscribe = $request->query->get('email');
        return $this->render('contact/index.html.twig', [
          'unsubscribe' => $unsubscribe
        ]);
    }

    #[Route('/contact/api', name: 'api')]
    public function api(Request $request, MailerInterface $mailer): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $receivers = ['schochal@student.ethz.ch', 'alvgarcia@student.ethz.ch', 'gbelleri@student.ethz.ch', 'dmitriy.pivovarov@mat.ethz.ch'];

        $email = (new Email())
          ->from('Alexander Schoch <schochal@student.ethz.ch>')
          ->to(...$receivers)
          ->replyTo($data->email)
          ->subject('[Group Blue] Message Request')
          ->text($data->message);

        $mailer->send($email);

        return new Response('Success');
    }

    #[Route('/contact/apiN', name: 'apiN')]
    public function apiN(Request $request, SubscriberRepository $sr): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $em = $this->getDoctrine()->getManager();

        if($sr->findOneByEmail($data->email) != null) {
          return new Response("You've already subscribed to the newsletter");
        }

        $sub = new Subscriber();
        $sub->setName($data->name);
        $sub->setEmail($data->email);

        $em->persist($sub);
        $em->flush();

        return new Response('You are now subscribed to the newsletter');
    }

    #[Route('/contact/apiU', name: 'apiU')]
    public function apiU(Request $request, SubscriberRepository $sr): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $em = $this->getDoctrine()->getManager();

        if($sr->findOneByEmail($data->email) == null) {
          return new Response("This email address is not subscribed to the newsletter");
        }

        $sub = $sr->findOneByEmail($data->email);
        $em->remove($sub);
        $em->flush();

        return new Response('You are now unsubscribed from the newsletter');
    }
}
