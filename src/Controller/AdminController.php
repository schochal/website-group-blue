<?php

namespace App\Controller;

use App\Repository\WebsiteHitRepository;
use App\Repository\SubscriberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    #[Route('/admin/api', name: 'adminApi')]
    public function adminApi(SubscriberRepository $sr): Response
    {
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers);
        
        $subscribers = $sr->findAll();
        return new JsonResponse($serializer->normalize($subscribers));
    }

    #[Route('/admin/websiteHits/{days}', name: 'websiteHits')]
    public function websiteHits(WebsiteHitRepository $whr, int $days): JsonResponse
    {
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer];
        $serializer = new Serializer($normalizers);
        $end = new \DateTime();
        if($days == 1) {
          $end->modify('+1 hour');
          $start = clone $end;
          $start->modify('-1 day');
          $interval = \DateInterval::createFromDateString('1 hour');
          $period = new \DatePeriod($start, $interval, $end);
          $startString = $start->format('Y-m-d H' . ':00:00');
        } else {
          $start = clone $end;
          $start->modify('-' . $days-1 . ' days');
          $end->modify('+1 day');
          $interval = \DateInterval::createFromDateString('1 day');
          $period = new \DatePeriod($start, $interval, $end);
          $startString = $start->format('Y-m-d ' . '00:00:00');
        }

        $data = $whr->findNewerThan($startString);

        $labels = array();
        foreach($period as $dt) {
          if($days == 1) {
            $labelStr = $dt->format('Y-m-d H' . ':00:00');
          } else {
            $labelStr = $dt->format('Y-m-d');
          }
          array_push($labels, $labelStr);
        }

        $main = array_fill_keys($labels, 0);
        $about = array_fill_keys($labels, 0);
        $contact = array_fill_keys($labels, 0);
        $hits = array_fill_keys($labels, 0);

        if($days == 1) {
          foreach($data as $entry) {
            $hits[$entry->getTimestamp()->format('Y-m-d H') . ':00:00'] += 1;
            switch($entry->getSite()) {
              case 'main':
                $main[$entry->getTimestamp()->format('Y-m-d H') . ':00:00'] += 1;
                break;
              case 'about':
                $about[$entry->getTimestamp()->format('Y-m-d H') . ':00:00'] += 1;
                break;
              case 'contact':
                $contact[$entry->getTimestamp()->format('Y-m-d H') . ':00:00'] += 1;
                break;
            }
          }
        } else {
          foreach($data as $entry) {
            $hits[$entry->getTimestamp()->format('Y-m-d')] += 1;
            switch($entry->getSite()) {
              case 'main':
                $main[$entry->getTimestamp()->format('Y-m-d')] += 1;
                break;
              case 'about':
                $about[$entry->getTimestamp()->format('Y-m-d')] += 1;
                break;
              case 'contact':
                $contact[$entry->getTimestamp()->format('Y-m-d')] += 1;
                break;
            }
          }
        }

        $obj = array(
          'hits' => $hits,
          'main' => $main,
          'about' => $about,
          'contact' => $contact,
          'labels' => $labels
        );

        return new JsonResponse($serializer->normalize($obj));
    }

    #[Route('/admin/userAgent', name: 'userAgent')]
    public function userAgent(WebsiteHitRepository $whr): JsonResponse
    {
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer];
        $serializer = new Serializer($normalizers);

        $data = $whr->findAll();

        $resultsB = array(
          'Internet Explorer' => 0,
          'Chrome' => 0,
          'Edge' => 0,
          'Firefox' => 0,
          'Opera' => 0,
          'Safari' => 0,
          'Other' => 0,
        );

        $resultsOS = array(
          'Linux' => 0,
          'MacOS' => 0,
          'iOS' => 0,
          'Android' => 0,
          'UNIX' => 0,
          'Windows' => 0,
          'Other' => 0,
        );

        foreach($data as $entry) {
          $ua = $entry->getUserAgent();
          if( preg_match('/MSIE (\d+\.\d+);/', $ua) ) {
            $resultsB['Internet Exporer'] += 1;
          } else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $ua) ) {
            $resultsB['Chrome'] += 1;
          } else if (preg_match('/Edge\/\d+/', $ua) ) {
            $resultsB['Edge'] += 1;
          } else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $ua) ) {
            $resultsB['Firefox'] += 1;
          } else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $ua) ) {
            $resultsB['Opera'] += 1;
          } else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $ua) ) {
            $resultsB['Safari'] += 1;
          } else {
            $resultsB['Other'] += 1;
          }

          if(preg_match('/droid/i',$ua)) $resultsOS['Android'] += 1; 
            elseif(preg_match('/Linux/i',$ua)) $resultsOS['Linux'] += 1;
            elseif(preg_match('/iPhone/i',$ua)) $resultsOS['iOS'] += 1; 
            elseif(preg_match('/iPad/i',$ua)) $resultsOS['iOS'] += 1; 
            elseif(preg_match('/Mac/i',$ua)) $resultsOS['MacOS'] += 1; 
            elseif(preg_match('/Unix/i',$ua)) $resultsOS['UNIX'] += 1; 
            elseif(preg_match('/Windows/i',$ua)) $resultsOS['Windows'] += 1;
            else $resultsOS['Other'] += 1;
        }

        return new JsonResponse($serializer->normalize(array(
          'BrowserKeys' => array_keys($resultsB),
          'BrowserValues' => array_values($resultsB),
          'OSKeys' => array_keys($resultsOS),
          'OSValues' => array_values($resultsOS),
        )));
    }
}
