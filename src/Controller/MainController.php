<?php

namespace App\Controller;

use App\Controller\AnalyticsController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(Request $request): Response
    {
        $analytics = new AnalyticsController();
        $analytics->index($this->getDoctrine()->getManager(), $request, 'main'); 
        return $this->render('main/index.html.twig');
    }
}
