<?php

namespace App\Entity;

use App\Repository\WebsiteHitRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WebsiteHitRepository::class)
 */
class WebsiteHit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Timestamp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $UserAgent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $site;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->Timestamp;
    }

    public function setTimestamp(\DateTimeInterface $Timestamp): self
    {
        $this->Timestamp = $Timestamp;

        return $this;
    }

    public function getUserAgent(): ?string
    {
        return $this->UserAgent;
    }

    public function setUserAgent(string $UserAgent): self
    {
        $this->UserAgent = $UserAgent;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(string $site): self
    {
        $this->site = $site;

        return $this;
    }
}
