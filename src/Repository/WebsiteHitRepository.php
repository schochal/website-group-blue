<?php

namespace App\Repository;

use App\Entity\WebsiteHit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WebsiteHit|null find($id, $lockMode = null, $lockVersion = null)
 * @method WebsiteHit|null findOneBy(array $criteria, array $orderBy = null)
 * @method WebsiteHit[]    findAll()
 * @method WebsiteHit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebsiteHitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebsiteHit::class);
    }

    /**
     * @return WebsiteHit[] Returns an array of WebsiteHit objects
     */
    public function findNewerThan($value)
    {
      return $this->createQueryBuilder('w')
                  ->andWhere('w.Timestamp >= :val')
                  ->setParameter('val', $value)
                  ->orderBy('w.Timestamp', 'ASC')
                  ->getQuery()
                  ->getResult()
      ;
    }

    // /**
    //  * @return WebsiteHit[] Returns an array of WebsiteHit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WebsiteHit
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
