# Website Group Blue

Website for Group Blue of the Chemical Product Design Course. It can be found
on https://group-blue.aschoch.ch

## Unsubscribe

It is possible to unsubscribe from the newsletter by visiting https://group-blue.aschoch.ch/contact?email=schochal@student.ethz.ch#unsubscribe
